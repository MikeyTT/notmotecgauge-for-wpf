﻿//************************************************
//** notMotecGauge for WPF written by MikeyTT
//**
//** 
//** based on math logic from geforz notMotec for Android: https://bitbucket.org/geforz/pcars-notmotec-for-android/src/dd75dbfdd1ae004c841277afa4af87831fec027d/src/de/kamelstall/pcarsdashandroid/view/Gauge.java?at=master
//** initial WPF help from DW12345 on the pCars forum
//** assistance from colets on the math functions and some sample code for WPF layout
//**
//** the CircularGauge implementation found here: http://www.codeproject.com/Articles/38361/Circular-gauge-custom-control-for-Silverlight-3-an
//** helped me understand dependencies, etc. Big thanks...
//** 
//** 
//** final conversion and adding the wow factor by MikeyTT
//**
//** 
//** usage:
//**
//** ValueMin:
//**    min value for the gauge
//**    currently only works properly if you select "0"
//** 
//** ValueMax:
//**    max value for the gauge
//**    have tested up to 55000 and it seems to be ok
//** 
//** ValueCurrent:
//**    value of the rpm bars
//**    best to set this via a binding to a datasource
//** 
//** ScaleLabelOffset:
//**    this adjusts the delta of the scale label
//** 
//** ScaleLabelFontSize:
//**    do really have to explain this?
//** 
//** ScaleLabelForeground:
//**    set the color
//** 
//** ScaleLabelMultiplier: 
//**    this tries to set the jumps for the scale labels
//**    a value of 1000, will make the scale ticks go up in 1000 blocks
//**    if this is too small a value then it will auto scale
//**    have a play with this, as sometimes you get odd results
//**    in theory you could go up in blocks of 2500 or 250
//** 
//** ScaleLabelRoundTo: 
//**    this scales the labels with this value
//**    if this is 1000 then it essentially does a value/1000 to get your label
//**    Can help to reduce the size of the displayed label
//** 
//** ScaleLabelDivisionsCount:
//**    start point for the amount of ticks
//**    this will get overridden if there are too many
//** 
//** ScaleTickColor:
//**    set the color
//** 
//** ScaleTickType:
//**    sets the default layout for the ticks on the gauge
//**    SmallNotch: small notches over the range of the gauge
//**    RPMBarSize: match the size of the rpm range with the ticks
//**    RPMBarOverSize: slightly larger than the rpm range with the ticks
//** 
//** ScaleMaxBarParts:
//**    sets the amount of bars over the entire rev range
//**    too few and it'll look very blocky
//**    75 - 100 look pleasing
//** 
//** ScaleMaxBarGap:
//**    gap between the rev bars
//**    behind the scenes the value is shrunk so it's not a pixel value or anything like that
//** 
//** RPMLineColor:
//**     set the RPM color
//** 
//** RedLineStyle="RPMAll"
//**    sets the style used for the red line
//**    RPMTip = change just the tip of the rpm range when in the redline 
//**    RPMAll = change the color of the entire rpm range when in the redline
//**    None = no redline at all
//** 
//** RedLineColor:
//**    color of the redline area
//** 
//** RedLineStartValue:
//**    value to start the redline area
//**    
//** GaugeText:
//**    generic placeholder if you want to have some text displayed
//** 
//** GaugeTextFontSize:
//**    the font size, funnily enough
//** 
//** GaugeTextColor:
//**    color of the text
//** 
//** GaugeTextLeftOffset:
//**    left offset on the canvas that the textblock is sitting in the generic.xaml
//** 
//** GaugeTextTopOffset:
//**    top offset on the canvas that the textblock is sitting in the generic.xaml
//** 
//** GaugeTextFontFamily:
//**    font for the textblock
//** 
//** GaugeStyle:
//**    orientation of the gauge skeleton
//**    Top: sets the outline (skeleton) to the top of the rpm range
//**    Bottom: sets the outline (skeleton) to the bottom of the rpm range
//**
//** GaugeType:
//**    chooses from a predefined list of gauge types. Some more useful than others
//**    if I'm honest. The Weird and Arc are there for show for now. Need to work on the points for these
//**    UserDefined will then allow the control to define the 4 points for the bezier curve
//**    it needs to be used with a modicum of care as you'll end up with some very odd shapes
//**
//** GaugeBezierPoint1:
//** GaugeBezierPoint2:
//** GaugeBezierPoint3:
//** GaugeBezierPoint4:
//**    defines the points to use when the gauge type is set to UserDefined
//**
//**
//**
//**************************************************************
//

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace notMotecGauge
{

    public enum eScaleTickType
    {
        SmallNotch,
        RPMBarSize,
        RPMBarOverSize
    }

    public enum eGaugeStyle
    {
        Bottom,
        Top
    }

    public enum eGaugeType
    {
        Motec = 0,
        MotecAlternate,
        Flat,
        Up,
        Circular,
        Weird,
        UserDefined
    }

    public enum eRedLineStyle
    {
        RPMTip,
        RPMAll,
        None
    }

    [TemplatePart(Name = "LayoutForGaugeSkeleton", Type = typeof(Grid))]
    [TemplatePart(Name = "LayoutForTicks", Type = typeof(Grid))]
    [TemplatePart(Name = "LayoutForLabels", Type = typeof(Grid))]
    [TemplatePart(Name = "LayoutForRPM", Type = typeof(Grid))]

    public class notMotecGaugeControl : Control
    {
        private Grid gridGaugeSkeleton;
        private Grid gridTicks;
        private Grid gridTickLabels;
        private Grid gridRPM;
        private LineSegment[] lineSegments_LS1;
        private LineSegment[] lineSegments_LS2;
        private LineSegment[] lineSegments_LS3;
        private LineSegment[] lineSegments_LS4;
        private LineSegment[] lineSegments_LS1_Tick;
        private LineSegment[] lineSegments_LS2_Tick;
        private LineSegment[] lineSegments_LS3_Tick;
        private LineSegment[] lineSegments_LS4_Tick;
        private Point[] gaugeScaleTickLabelPosition;
        private double redLinePoint;

        private float ax;
        private float ay;
        private float bx;
        private float by;
        private float cx;
        private float cy;
        private float dx;
        private float dy;
        
        //define the base gauge types
        //userdefined is not included as it will take the values from the control itself
        private float[,] gaugeTypeValues = new float[6, 8]{
            {50, 310, 40, 180, 140, 75, 475, 90}, //set the ax, ay, bx, by, cx, cy, dx, dy values for the gauge type eGaugeType.Motec
            {50, 310, 40, 190, 120, 90, 475, 90}, //set the ax, ay, bx, by, cx, cy, dx, dy values for the gauge type eGaugeType.MotecAlternate
            {0, 310, 80, 310, 200, 310, 475, 310}, //set the ax, ay, bx, by, cx, cy, dx, dy values for the gauge type eGaugeType.Flat
            {50, 310, 210, 275, 445, 275, 475, 50}, //set the ax, ay, bx, by, cx, cy, dx, dy values for the gauge type eGaugeType.Up
            {50, 310, 50, 50, 300, 50, 300, 310}, //set the ax, ay, bx, by, cx, cy, dx, dy values for the gauge type eGaugeType.Circular
            {50, 160, 150, 460, 330, -70, 475, 170} //set the ax, ay, bx, by, cx, cy, dx, dy values for the gauge type eGaugeType.Weird
        };

        private const float inner = 0.01f;
        private float outerTickMin = 0.05f;
        private float outerTickMax = 0.08f;

        static notMotecGaugeControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(notMotecGaugeControl), new FrameworkPropertyMetadata(typeof(notMotecGaugeControl)));
        }

        //scale dependency properties
        //
        public eScaleTickType ScaleTickType
        {
            get { return (eScaleTickType)GetValue(ScaleTickTypeProperty); }
            set { SetValue(ScaleTickTypeProperty, value); }
        }

        public static readonly DependencyProperty ScaleTickTypeProperty = DependencyProperty.Register("ScaleTickType", typeof(eScaleTickType), typeof(notMotecGaugeControl), new PropertyMetadata(null));
        public static readonly DependencyProperty ScaleTickColorProperty = DependencyProperty.Register("ScaleTickColor", typeof(Color), typeof(notMotecGaugeControl), null);

        public static readonly DependencyProperty ScaleMaxBarPartsProperty = DependencyProperty.Register("ScaleMaxBarParts", typeof(double), typeof(notMotecGaugeControl), null);
        public static readonly DependencyProperty ScaleMaxBarGapProperty = DependencyProperty.Register("ScaleMaxBarGap", typeof(double), typeof(notMotecGaugeControl), null);

        public static readonly DependencyProperty ScaleLabelMultiplierProperty =
            DependencyProperty.Register("ScaleLabelMultiplier", typeof(double), typeof(notMotecGaugeControl),
            new PropertyMetadata(double.Parse("1"), notMotecGaugeControl.OnScaleLabelMultiplierPropertyChanged));

        public static readonly DependencyProperty ScaleLabelOffsetProperty = DependencyProperty.Register("ScaleLabelOffset", typeof(double), typeof(notMotecGaugeControl), null);
        public static readonly DependencyProperty ScaleLabelFontSizeProperty = DependencyProperty.Register("ScaleLabelFontSize", typeof(double), typeof(notMotecGaugeControl), null);
        public static readonly DependencyProperty ScaleLabelForegroundProperty = DependencyProperty.Register("ScaleLabelForeground", typeof(Color), typeof(notMotecGaugeControl), null);
        public static readonly DependencyProperty ScaleLabelRoundToProperty = DependencyProperty.Register("ScaleLabelRoundTo", typeof(double), typeof(notMotecGaugeControl), null);
        public static readonly DependencyProperty ScaleLabelDivisionsCountProperty = DependencyProperty.Register("ScaleLabelDivisionsCount", typeof(double), typeof(notMotecGaugeControl), null);
        public static readonly DependencyProperty ScaleLabelFontFamilyProperty = DependencyProperty.Register("ScaleLabelFontFamily", typeof(FontFamily), typeof(notMotecGaugeControl), null);

        public eGaugeType GaugeType
        {
            get { return (eGaugeType)GetValue(GaugeTypeProperty); }
            set { SetValue(GaugeTypeProperty, value); }
        }

        public static readonly DependencyProperty GaugeTypeProperty = DependencyProperty.Register("GaugeType", typeof(eGaugeType), typeof(notMotecGaugeControl), new PropertyMetadata(null));

        public eGaugeStyle GaugeStyle
        {
            get { return (eGaugeStyle)GetValue(GaugeStyleProperty); }
            set { SetValue(GaugeStyleProperty, value); }
        }

        public static readonly DependencyProperty GaugeStyleProperty = DependencyProperty.Register("GaugeStyle", typeof(eGaugeStyle), typeof(notMotecGaugeControl), new PropertyMetadata(null));
        public static readonly DependencyProperty GaugeColorProperty = DependencyProperty.Register("GaugeColor", typeof(Color), typeof(notMotecGaugeControl), null);
        public static readonly DependencyProperty GaugeTextProperty = DependencyProperty.Register("GaugeText", typeof(string), typeof(notMotecGaugeControl), null);
        public static readonly DependencyProperty GaugeTextColorProperty = DependencyProperty.Register("GaugeTextColor", typeof(Color), typeof(notMotecGaugeControl), null);
        public static readonly DependencyProperty GaugeTextFontSizeProperty = DependencyProperty.Register("GaugeTextFontSize", typeof(double), typeof(notMotecGaugeControl), null);
        public static readonly DependencyProperty GaugeTextFontFamilyProperty = DependencyProperty.Register("GaugeTextFontFamily", typeof(FontFamily), typeof(notMotecGaugeControl), null);
        public static readonly DependencyProperty GaugeTextLeftOffsetProperty = DependencyProperty.Register("GaugeTextLeftOffset", typeof(double), typeof(notMotecGaugeControl), null);
        public static readonly DependencyProperty GaugeTextTopOffsetProperty = DependencyProperty.Register("GaugeTextTopOffset", typeof(double), typeof(notMotecGaugeControl), null);
        public static readonly DependencyProperty GaugeMinWidthProperty = DependencyProperty.Register("GaugeMinWidth", typeof(double), typeof(notMotecGaugeControl), null);
        public static readonly DependencyProperty GaugeMaxWidthProperty = DependencyProperty.Register("GaugeMaxWidth", typeof(double), typeof(notMotecGaugeControl), null);
        public static readonly DependencyProperty GaugeBezierPoint1Property = DependencyProperty.Register("GaugeBezierPoint1", typeof(Point), typeof(notMotecGaugeControl), null);
        public static readonly DependencyProperty GaugeBezierPoint2Property = DependencyProperty.Register("GaugeBezierPoint2", typeof(Point), typeof(notMotecGaugeControl), null);
        public static readonly DependencyProperty GaugeBezierPoint3Property = DependencyProperty.Register("GaugeBezierPoint3", typeof(Point), typeof(notMotecGaugeControl), null);
        public static readonly DependencyProperty GaugeBezierPoint4Property = DependencyProperty.Register("GaugeBezierPoint4", typeof(Point), typeof(notMotecGaugeControl), null);
        
        public static readonly DependencyProperty RPMLineColorProperty = DependencyProperty.Register("RPMLineColor", typeof(Color), typeof(notMotecGaugeControl), null);

        //red line dependency properties
        public eRedLineStyle RedLineStyle
        {
            get { return (eRedLineStyle)GetValue(RedLineStyleProperty); }
            set { SetValue(RedLineStyleProperty, value); }
        }

        public static readonly DependencyProperty RedLineStyleProperty = DependencyProperty.Register("RedLineStyle", typeof(eRedLineStyle), typeof(notMotecGaugeControl), new PropertyMetadata(null));
        public static readonly DependencyProperty RedLineColorProperty = DependencyProperty.Register("RedLineColor", typeof(Color), typeof(notMotecGaugeControl), null);
        public static readonly DependencyProperty RedLineStartValueProperty =
           DependencyProperty.Register("RedLineStartValue", typeof(double), typeof(notMotecGaugeControl),
           new PropertyMetadata(Double.MaxValue, notMotecGaugeControl.OnRedLineStartValuePropertyChanged));


        //value dependency properties
        public static readonly DependencyProperty ValueMinProperty = DependencyProperty.Register("ValueMin", typeof(double), typeof(notMotecGaugeControl), null);
        public static readonly DependencyProperty ValueCurrentProperty =
            DependencyProperty.Register("ValueCurrent", typeof(double), typeof(notMotecGaugeControl),
            new PropertyMetadata(Double.MinValue, notMotecGaugeControl.OnValueCurrentPropertyChanged));

        public static readonly DependencyProperty ValueMaxProperty =
            DependencyProperty.Register("ValueMax", typeof(double), typeof(notMotecGaugeControl),
            new PropertyMetadata(Double.MaxValue, notMotecGaugeControl.OnValueMaxPropertyChanged));


        public Color RPMLineColor
        {
            get { return (Color)GetValue(RPMLineColorProperty); }
            set { SetValue(RPMLineColorProperty, value); }
        }

        #region get/set scale related info
        public double ScaleLabelDivisionsCount
        {
            get { return (double)GetValue(ScaleLabelDivisionsCountProperty); }
            set { SetValue(ScaleLabelDivisionsCountProperty, value); }
        }

        public double ScaleLabelRoundTo
        {
            get { return (double)GetValue(ScaleLabelRoundToProperty); }
            set { SetValue(ScaleLabelRoundToProperty, value); }
        }

        public double ScaleLabelMultiplier
        {
            get { return (double)GetValue(ScaleLabelMultiplierProperty); }
            set { SetValue(ScaleLabelMultiplierProperty, value); }
        }

        public double ScaleMaxBarParts
        {
            get { return (double)GetValue(ScaleMaxBarPartsProperty); }
            set { SetValue(ScaleMaxBarPartsProperty, value); }
        }

        public double ScaleMaxBarGap
        {
            get { return (double)GetValue(ScaleMaxBarGapProperty); }
            set { SetValue(ScaleMaxBarGapProperty, value); }
        }

        public double ScaleLabelOffset
        {
            get { return (double)GetValue(ScaleLabelOffsetProperty); }
            set { SetValue(ScaleLabelOffsetProperty, value); }
        }

        public double ScaleLabelFontSize
        {
            get { return (double)GetValue(ScaleLabelFontSizeProperty); }
            set { SetValue(ScaleLabelFontSizeProperty, value); }
        }

        public FontFamily ScaleLabelFontFamily
        {
            get { return (FontFamily)GetValue(ScaleLabelFontFamilyProperty); }
            set { SetValue(ScaleLabelFontFamilyProperty, value); }
        }

        public Color ScaleLabelForeground
        {
            get { return (Color)GetValue(ScaleLabelForegroundProperty); }
            set { SetValue(ScaleLabelForegroundProperty, value); }
        }

        public Color ScaleTickColor
        {
            get { return (Color)GetValue(ScaleTickColorProperty); }
            set { SetValue(ScaleTickColorProperty, value); }
        }
#endregion


        #region get/set red line related info
        public double RedLineStartValue
        {
            get { return (double)GetValue(RedLineStartValueProperty); }
            set { SetValue(RedLineStartValueProperty, value); }
        }

        public Color RedLineColor
        {
            get { return (Color)GetValue(RedLineColorProperty); }
            set { SetValue(RedLineColorProperty, value); }
        }
#endregion

        #region get/set the values related info
        public double ValueCurrent
        {
            get { return (double)GetValue(ValueCurrentProperty); }
            set { SetValue(ValueCurrentProperty, value); }
        }

        public double ValueMin
        {
            get { return (double)GetValue(ValueMinProperty); }
            set { SetValue(ValueMinProperty, value); }
        }

        public double ValueMax
        {
            get { return (double)GetValue(ValueMaxProperty); }
            set { SetValue(ValueMaxProperty, value); }
        }
#endregion 
        

        #region get/set the gauge related info
        public Point GaugeBezierPoint1
        {
            get { return (Point)GetValue(GaugeBezierPoint1Property); }
            set { SetValue(GaugeBezierPoint1Property, value); }
        }

        public Point GaugeBezierPoint2
        {
            get { return (Point)GetValue(GaugeBezierPoint2Property); }
            set { SetValue(GaugeBezierPoint2Property, value); }
        }

        public Point GaugeBezierPoint3
        {
            get { return (Point)GetValue(GaugeBezierPoint3Property); }
            set { SetValue(GaugeBezierPoint3Property, value); }
        }

        public Point GaugeBezierPoint4
        {
            get { return (Point)GetValue(GaugeBezierPoint4Property); }
            set { SetValue(GaugeBezierPoint4Property, value); }
        }

        public double GaugeMinWidth
        {
            get { return (double)GetValue(GaugeMinWidthProperty); }
            set { SetValue(GaugeMinWidthProperty, value); }
        }

        public double GaugeMaxWidth
        {
            get { return (double)GetValue(GaugeMaxWidthProperty); }
            set { SetValue(GaugeMaxWidthProperty, value); }
        }

        public double GaugeTextLeftOffset
        {
            get { return (double)GetValue(GaugeTextLeftOffsetProperty); }
            set { SetValue(GaugeTextLeftOffsetProperty, value); }
        }

        public double GaugeTextTopOffset
        {
            get { return (double)GetValue(GaugeTextTopOffsetProperty); } 
            set { SetValue(GaugeTextTopOffsetProperty, value); }
        }

        public FontFamily GaugeTextFontFamily
        {
            get { return (FontFamily)GetValue(GaugeTextFontFamilyProperty); }
            set { SetValue(GaugeTextFontFamilyProperty, value); }
        }

        public string GaugeText
        {
            get { return (string)GetValue(GaugeTextProperty); }
            set { SetValue(GaugeTextProperty, value); }
        }

        public Color GaugeTextColor
        {
            get { return (Color)GetValue(GaugeTextColorProperty); }
            set { SetValue(GaugeTextColorProperty, value); }
        }

        public double GaugeTextFontSize
        {
            get { return (double)GetValue(GaugeTextFontSizeProperty); }
            set { SetValue(GaugeTextFontSizeProperty, value); }
        }
#endregion

        #region methods that do the real work

        private static void OnValueCurrentPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            notMotecGaugeControl gauge = d as notMotecGaugeControl;
            gauge.OnValueCurrentChanged(e);
        }

        private static void OnScaleLabelMultiplierPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            notMotecGaugeControl gauge = d as notMotecGaugeControl;
            gauge.OnMultiplierChanged(e);
        }

        private static void OnValueMaxPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            notMotecGaugeControl gauge = d as notMotecGaugeControl;
            gauge.OnValueMaxChanged(e);
        }

        private static void OnRedLineStartValuePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            notMotecGaugeControl gauge = d as notMotecGaugeControl;
            gauge.OnRedLineStartValueChanged(e);
        }

        public virtual void OnMultiplierChanged(DependencyPropertyChangedEventArgs e)
        {
            double newValue = (double)e.NewValue;
            double oldValue = (double)e.OldValue;

            if ((newValue <= 0) || (double.IsInfinity(newValue)) || (double.IsNaN(newValue)))
            {
                ScaleLabelMultiplier = oldValue;
                newValue = oldValue;
            }

            RedrawGaugeSkeleton(e);
        }

        public virtual void RedrawGaugeSkeleton(DependencyPropertyChangedEventArgs e)
        {
            if (gridGaugeSkeleton != null)
            {
                DrawGaugeSkeleton();
            }
        }

        public virtual void OnRedLineStartValueChanged(DependencyPropertyChangedEventArgs e)
        {
            double newValue = (double)e.NewValue;
            double oldValue = (double)e.OldValue;

            if ((newValue == 0) || (double.IsInfinity(newValue)) || (double.IsNaN(newValue)))
            {
                RedLineStartValue = oldValue;
                newValue = oldValue;
            }

            RedrawGaugeSkeleton(e);
        }
        public virtual void OnValueMaxChanged(DependencyPropertyChangedEventArgs e)
        {
            double newValue = (double)e.NewValue;
            double oldValue = (double)e.OldValue;

            if ((newValue == 0) || (double.IsInfinity(newValue)) || (double.IsNaN(newValue)))
            {
                ValueMax = oldValue;
                newValue = oldValue;
            }

            RedrawGaugeSkeleton(e);
        }

        public virtual void OnValueCurrentChanged(DependencyPropertyChangedEventArgs e)
        {
            if (ScaleLabelMultiplier == 0)
                ScaleLabelMultiplier = 1;

            double newValue = (double)e.NewValue;
            double oldValue = (double)e.OldValue;

            if (newValue > ValueMax)
            {
                newValue = ValueMax;
            }
            else if (newValue < ValueMin)
            {
                newValue = ValueMin;
            }

            if (oldValue > ValueMax)
            {
                oldValue = ValueMax;
            }
            else if (oldValue < ValueMin)
            {
                oldValue = ValueMin;
            }

            if (gridRPM != null)
            {
                DrawRPM();
            }
        }

        private void PlotAllPoints()
        {
            //convert dependency objects to local variables as they process much faster
            eScaleTickType _majorScaleTickType = ScaleTickType;
            eGaugeStyle _gaugeStyle = GaugeStyle;
            eGaugeType _gaugeType = GaugeType;

            double _scaleLabelOffset = ScaleLabelOffset / 1000;
            double _redLineStartValue = RedLineStartValue;
            double _scaleMaxBarParts = ScaleMaxBarParts;
            double _scaleMaxBarGap = ScaleMaxBarGap;
            double _valueMax = ValueMax;
            float _gaugeMinWidth = (float)GaugeMinWidth / 100; //in the control you set 2, that needs to translate to 0.02
            float _gaugeMaxWidth = (float)GaugeMaxWidth / 100;

            //if the redline start value is less than 100 then assume it's a percentage and scale the 
            //redline start point as a percentage instead
            if (_redLineStartValue < 100)
            {
                _redLineStartValue = (_valueMax / 100) * _redLineStartValue;
            }


            //set red line point for the scale
            redLinePoint = Math.Round(((_scaleMaxBarParts / 100) * ((_redLineStartValue / _valueMax) * 100)), 0, MidpointRounding.ToEven);

            if (_gaugeType == eGaugeType.UserDefined)
            {
                //if the gauge is set as user defined then use these values from the control instead
                ax = (float)GaugeBezierPoint1.X;
                ay = (float)GaugeBezierPoint1.Y;
                bx = (float)GaugeBezierPoint2.X;
                by = (float)GaugeBezierPoint2.Y;
                cx = (float)GaugeBezierPoint3.X;
                cy = (float)GaugeBezierPoint3.Y;
                dx = (float)GaugeBezierPoint4.X;
                dy = (float)GaugeBezierPoint4.Y;
            }
            else
            {
                //set the ax, by, etc. values based on the type of gauge
                ax = gaugeTypeValues[(int)GaugeType, 0];
                ay = gaugeTypeValues[(int)GaugeType, 1];
                bx = gaugeTypeValues[(int)GaugeType, 2];
                by = gaugeTypeValues[(int)GaugeType, 3];
                cx = gaugeTypeValues[(int)GaugeType, 4];
                cy = gaugeTypeValues[(int)GaugeType, 5];
                dx = gaugeTypeValues[(int)GaugeType, 6];
                dy = gaugeTypeValues[(int)GaugeType, 7];
            }

            float blFactor;
            float blPx;
            float blPy;
            float blVx;
            float blVy;
            float blSx;
            float blSy;
            float buFactor;
            float buPx;
            float buPy;
            float buVx;
            float buVy;

            for (int arc = 0; arc < (int)_scaleMaxBarParts; arc++)
            {
                blFactor = (arc) / (float)_scaleMaxBarParts;

                blPx = bezierPoint(blFactor, ax, bx, cx, dx);
                blPy = bezierPoint(blFactor, ay, by, cy, dy);

                blVx = bezierTangent(blFactor, ax, bx, cx, dx);
                blVy = bezierTangent(blFactor, ay, by, cy, dy);

                blSx = blVy;
                blSy = -blVx;

                buFactor = ((arc) + 1) / (float)_scaleMaxBarParts - (float)(_scaleMaxBarGap / 1000);
                buPx = bezierPoint(buFactor, ax, bx, cx, dx);
                buPy = bezierPoint(buFactor, ay, by, cy, dy);

                buVx = bezierTangent(buFactor, ax, bx, cx, dx);
                buVy = bezierTangent(buFactor, ay, by, cy, dy);

                //store the calculated values so you don't need to keep calculating them
                lineSegments_LS1[arc] = new LineSegment()
                {
                    Point = new Point(
                        blPx + ((inner + _gaugeMinWidth + _gaugeMaxWidth * blFactor) * blVy),
                        blPy + ((inner + _gaugeMinWidth + _gaugeMaxWidth * blFactor) * -blVx))
                };

                lineSegments_LS2[arc] = new LineSegment()
                {
                    Point = new Point(
                        buPx + ((inner + _gaugeMinWidth + _gaugeMaxWidth * blFactor) * buVy),
                        buPy + ((inner + _gaugeMinWidth + _gaugeMaxWidth * blFactor) * -buVx))
                };

                lineSegments_LS3[arc] = new LineSegment()
                {
                    Point = new Point(
                        buPx + ((inner) * buVy),
                        buPy + ((inner) * -buVx))
                };

                lineSegments_LS4[arc] = new LineSegment()
                {
                    Point = new Point(
                        blPx + ((inner) * blVy),
                        blPy + ((inner) * -blVx))
                };

                //set the position of the scale label
                gaugeScaleTickLabelPosition[arc] = new Point()
                {
                    X = buPx + ((inner + _scaleLabelOffset) * buVy),
                    Y = buPy + ((inner + _scaleLabelOffset) * -buVx)
                };

                //depending on the way the gauge is setup will depend on what
                //values we have to massage from the default LS1-LS4 positions
                switch (_majorScaleTickType)
                {
                    case eScaleTickType.SmallNotch:
                        if (_gaugeStyle == eGaugeStyle.Bottom)
                        {
                            //force a small notch size - could make it a dependency I suppose
                            outerTickMin = 0.015f;
                            outerTickMax = 0.0001f;
                        }
                        else
                        {
                            //if we're on the top we have to do things a little differently
                            outerTickMin = 0.02f;
                            outerTickMax = 0.02f; //_gaugeMaxWidth; <-- goes weird if you use this, when it's set on the control and the control has some low values
                        }
                        break;

                    case eScaleTickType.RPMBarSize:
                        //this keeps the rpm the same size as the bars, or the other way around
                        outerTickMin = _gaugeMinWidth;
                        outerTickMax = _gaugeMaxWidth;
                        break;

                    case eScaleTickType.RPMBarOverSize:
                        //need to add a little to oversize the tick bars
                        if (_gaugeStyle == eGaugeStyle.Bottom)
                        {
                            outerTickMin = _gaugeMinWidth + 0.008f;
                            outerTickMax = _gaugeMaxWidth;
                        }
                        else
                        {
                            //we're upside down so need to subtract a bit
                            outerTickMin = -0.006f;
                            outerTickMax = -0.004f;
                        }

                        break;
                }

                switch (_gaugeStyle)
                {
                    case eGaugeStyle.Bottom:
                        //this is the easy one and the very similar code to the Android version
                        lineSegments_LS1_Tick[arc] = new LineSegment()
                        {
                            Point = new Point(
                                blPx + ((inner + outerTickMin + outerTickMax * blFactor) * blVy),
                                blPy + ((inner + outerTickMin + outerTickMax * blFactor) * -blVx))
                        };

                        lineSegments_LS2_Tick[arc] = new LineSegment()
                        {
                            Point = new Point(
                                buPx + ((inner + outerTickMin + outerTickMax * blFactor) * buVy),
                                buPy + ((inner + outerTickMin + outerTickMax * blFactor) * -buVx))
                        };

                        lineSegments_LS3_Tick[arc] = new LineSegment()
                        {
                            Point = new Point(
                                buPx + ((inner) * buVy),
                                buPy + ((inner) * -buVx))
                        };

                        lineSegments_LS4_Tick[arc] = new LineSegment()
                        {
                            Point = new Point(
                                blPx + ((inner) * blVy),
                                blPy + ((inner) * -blVx))
                        };
                        break;

                    case eGaugeStyle.Top:
                        lineSegments_LS1_Tick[arc] = new LineSegment()
                        {
                            Point = new Point(
                            blPx + ((inner + _gaugeMinWidth + _gaugeMaxWidth * blFactor) * blVy),
                            blPy + ((inner + _gaugeMinWidth + _gaugeMaxWidth * blFactor) * -blVx))
                        };

                        lineSegments_LS2_Tick[arc] = new LineSegment()
                        {
                            Point = new Point(
                            buPx + ((inner + _gaugeMinWidth + _gaugeMaxWidth * blFactor) * buVy),
                            buPy + ((inner + _gaugeMinWidth + _gaugeMaxWidth * blFactor) * -buVx))
                        };

                        switch (_majorScaleTickType)
                        {
                            case eScaleTickType.SmallNotch:
                            case eScaleTickType.RPMBarOverSize:
                                lineSegments_LS3_Tick[arc] = new LineSegment()
                                {
                                    Point = new Point(
                                        buPx + ((inner + outerTickMin + outerTickMax * blFactor) * buVy),
                                        buPy + ((inner + outerTickMin + outerTickMax * blFactor) * -buVx))
                                };

                                lineSegments_LS4_Tick[arc] = new LineSegment()
                                {
                                    Point = new Point(
                                        blPx + ((inner + outerTickMin + outerTickMax * blFactor) * blVy),
                                        blPy + ((inner + outerTickMin + outerTickMax * blFactor) * -blVx))
                                };
                                break;

                            case eScaleTickType.RPMBarSize:
                                lineSegments_LS3_Tick[arc] = new LineSegment()
                                {
                                    Point = new Point(
                                        buPx + ((inner) * buVy),
                                        buPy + ((inner) * -buVx))
                                };

                                lineSegments_LS4_Tick[arc] = new LineSegment()
                                {
                                    Point = new Point(
                                        blPx + ((inner) * blVy),
                                        blPy + ((inner) * -blVx))
                                };
                                break;
                        }
                        break;
                }
            }
        }

        void DrawRPM()
        {
            //convert dependency objects to local variables as they process much faster
            double _valueCurrent = ValueCurrent;
            double _valueMax = ValueMax;
            eRedLineStyle _redLineStyle = RedLineStyle;
            Color _redLineColor = RedLineColor;
            Color _rpmLineColor = RPMLineColor;
            double _scaleMaxBarParts = ScaleMaxBarParts;

            double rpmPoint = Math.Round(((_scaleMaxBarParts / 100) * ((_valueCurrent / _valueMax) * 100)), 0, MidpointRounding.ToEven);

            if (rpmPoint > _scaleMaxBarParts)
                rpmPoint = _scaleMaxBarParts;

            PathGeometry[] rpmPaths = new PathGeometry[(int)rpmPoint];

            for (int arc = 0; arc < rpmPoint; arc++)
            {
                PathGeometry path_geometry = new PathGeometry();
                PathFigure path_figure = new PathFigure() { StartPoint = new Point(lineSegments_LS4[arc].Point.X, lineSegments_LS4[arc].Point.Y), IsClosed = true };
                path_geometry.Figures.Add(path_figure);

                path_figure.Segments.Add(lineSegments_LS1[arc]);
                path_figure.Segments.Add(lineSegments_LS2[arc]);
                path_figure.Segments.Add(lineSegments_LS3[arc]);
                path_figure.Segments.Add(lineSegments_LS4[arc]);

                rpmPaths[arc] = path_geometry;
            }

            int i = 0;
            gridRPM.Children.Clear();

            foreach (PathGeometry geom in rpmPaths)
            {
                Path p = new Path() { Data = geom, StrokeThickness = 2 };

                if (((i >= redLinePoint) && _redLineStyle != eRedLineStyle.None) || ((_redLineStyle == eRedLineStyle.RPMAll) && (rpmPoint >= redLinePoint)))
                {
                    //if RPMALL is selected then change the color of all the RPM range
                    //if not and we're in the redline are then just color the tip
                    p.Stroke = new SolidColorBrush(_redLineColor);
                    p.Fill = new SolidColorBrush(_redLineColor);
                }
                else
                {
                    p.Stroke = new SolidColorBrush(_rpmLineColor);
                    p.Fill = new SolidColorBrush(_rpmLineColor);
                }

                gridRPM.Children.Add(p);
                i++;
            }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            lineSegments_LS1 = new LineSegment[(int)ScaleMaxBarParts];
            lineSegments_LS2 = new LineSegment[(int)ScaleMaxBarParts];
            lineSegments_LS3 = new LineSegment[(int)ScaleMaxBarParts];
            lineSegments_LS4 = new LineSegment[(int)ScaleMaxBarParts];
            lineSegments_LS1_Tick = new LineSegment[(int)ScaleMaxBarParts];
            lineSegments_LS2_Tick = new LineSegment[(int)ScaleMaxBarParts];
            lineSegments_LS3_Tick = new LineSegment[(int)ScaleMaxBarParts];
            lineSegments_LS4_Tick = new LineSegment[(int)ScaleMaxBarParts];
            gaugeScaleTickLabelPosition = new Point[(int)ScaleMaxBarParts];

            //Get reference to known elements on the control template
            gridGaugeSkeleton = GetTemplateChild("LayoutForGaugeSkeleton") as Grid;
            gridTicks = GetTemplateChild("LayoutForTicks") as Grid;
            gridTickLabels = GetTemplateChild("LayoutForLabels") as Grid;
            gridRPM = GetTemplateChild("LayoutForRPM") as Grid;

            //draw gauge skeleton
            DrawGaugeSkeleton();
        }

        private void DrawGaugeSkeleton()
        {
            //re-calculate the base plot points for the gauge skeleton
            PlotAllPoints();
            if (gridTicks.Children.Count > 0)
                gridTicks.Children.Clear();

            if (gridGaugeSkeleton.Children.Count > 0)
                gridGaugeSkeleton.Children.Clear();

            if (gridTickLabels.Children.Count > 0)
                gridTickLabels.Children.Clear();


            //convert dependency objects to local variables as they process much faster
            //TODO: need to double check I got all dependency properties
            eGaugeStyle _gaugeStyle = GaugeStyle;
            eRedLineStyle _redLineStyle = RedLineStyle;
            eGaugeType _gaugeType = GaugeType;

            double _valueMax = ValueMax;
            double _valueMin = ValueMin;
            double _scaleLabelDivisionCount = ScaleLabelDivisionsCount;
            Color _redLineColor = RedLineColor;
            Color _majorScaleTickColor = ScaleTickColor;
            FontFamily _scaleLabelFontFamily = ScaleLabelFontFamily;
            int _maxBarParts = (int)ScaleMaxBarParts;

            double _stepPerDivision = 0;
            double _valuePerDivision = 0;
            double _valuePerDivisionMaxed = 0;
            double _newScaleMaxBarParts = _maxBarParts - 1;
            double _newMajorDivisionCount = ScaleLabelDivisionsCount;
            double _majorTicksUnitValue = 0;
            double _majorTickUnit = 0;

            _stepPerDivision = _maxBarParts / _newMajorDivisionCount;

            _valuePerDivision = (_valueMax - _valueMin) / _stepPerDivision;
            if (_valuePerDivision % ScaleLabelMultiplier != 0)
            {
                _valuePerDivisionMaxed = (_valuePerDivision - _valuePerDivision % ScaleLabelMultiplier) + ScaleLabelMultiplier;
            }
            else
            {
                _valuePerDivisionMaxed = _valuePerDivision;
            }

            if (((_valueMax - _valueMin) / _valuePerDivisionMaxed) < _scaleLabelDivisionCount)
            {
                //need less divisions
                _newMajorDivisionCount = (_valueMax - _valueMin) / _valuePerDivisionMaxed;
            }
            else
            {
                //need more divisions
                _newMajorDivisionCount = (_valueMax - _valueMin) / _valuePerDivisionMaxed;
            }

            _majorTicksUnitValue = (_valueMax - _valueMin) / _newMajorDivisionCount;
            _majorTickUnit = _newScaleMaxBarParts / _newMajorDivisionCount;

            PathGeometry[] tickPaths = new PathGeometry[(int)ScaleMaxBarParts];

            PointCollection rpmPoints = new PointCollection();
            PointCollection rpmRedLinePoints = new PointCollection();

            for (int arc = 0; arc < _maxBarParts; arc++)
            {
                if ((int)(arc % _majorTickUnit) == 0)
                {
                    PathGeometry path_geometry = new PathGeometry();
                    PathFigure path_figure = new PathFigure();

                    switch (_gaugeStyle)
                    {
                        case eGaugeStyle.Bottom:
                            path_figure.StartPoint = new Point(lineSegments_LS4[arc].Point.X, lineSegments_LS4[arc].Point.Y);
                            path_figure.Segments.Add(lineSegments_LS1_Tick[arc]);
                            path_figure.Segments.Add(lineSegments_LS2_Tick[arc]);
                            path_figure.Segments.Add(lineSegments_LS3_Tick[arc]);
                            path_figure.Segments.Add(lineSegments_LS4_Tick[arc]);
                            break;

                        case eGaugeStyle.Top:
                            path_figure.StartPoint = new Point(lineSegments_LS4_Tick[arc].Point.X, lineSegments_LS4_Tick[arc].Point.Y);
                            path_figure.Segments.Add(lineSegments_LS1_Tick[arc]);
                            path_figure.Segments.Add(lineSegments_LS2_Tick[arc]);
                            path_figure.Segments.Add(lineSegments_LS3_Tick[arc]);
                            path_figure.Segments.Add(lineSegments_LS4_Tick[arc]);
                            break;
                    }

                    path_figure.IsClosed = true;
                    path_geometry.Figures.Add(path_figure);

                    tickPaths[arc] = path_geometry;

                    TextBlock textBlock = new TextBlock() 
                    { 
                        FontFamily = _scaleLabelFontFamily,
                        FontSize = ScaleLabelFontSize, 
                        Foreground = new SolidColorBrush(ScaleLabelForeground), 
                        Text = Math.Round(_valueMin / ScaleLabelRoundTo, 0, MidpointRounding.AwayFromZero).ToString() 
                    };
                    _valueMin = _valueMin + _majorTicksUnitValue;

                    textBlock.RenderTransform = new TranslateTransform() { X = gaugeScaleTickLabelPosition[arc].X, Y = gaugeScaleTickLabelPosition[arc].Y };
                    gridTickLabels.Children.Add(textBlock);
                }

                switch (_gaugeStyle)
                {
                    case eGaugeStyle.Bottom:
                        if (arc >= redLinePoint)
                        {
                            rpmPoints.Add(lineSegments_LS4[arc].Point);
                            rpmRedLinePoints.Add(lineSegments_LS4[arc].Point);
                        }
                        else
                        {
                            rpmPoints.Add(lineSegments_LS4[arc].Point);
                        }
                        break;

                    case eGaugeStyle.Top:
                        if (arc >= redLinePoint)
                        {
                            rpmPoints.Add(lineSegments_LS1[arc].Point);
                            rpmRedLinePoints.Add(lineSegments_LS1[arc].Point);
                        }
                        else
                        {
                            rpmPoints.Add(lineSegments_LS1[arc].Point);
                        }
                        break;
                }
            }

            //add final segment to the path
            _maxBarParts--;
            switch (_gaugeStyle)
            {
                case eGaugeStyle.Bottom:
                    if (_maxBarParts >= redLinePoint)
                    {
                        rpmPoints.Add(lineSegments_LS3[_maxBarParts].Point);
                        rpmRedLinePoints.Add(lineSegments_LS3[_maxBarParts].Point);
                    }
                    else
                    {
                        rpmPoints.Add(lineSegments_LS3[_maxBarParts].Point);
                    }
                    break;

                case eGaugeStyle.Top:
                    if (_maxBarParts >= redLinePoint)
                    {
                        rpmPoints.Add(lineSegments_LS2[_maxBarParts].Point);
                        rpmRedLinePoints.Add(lineSegments_LS2[_maxBarParts].Point);
                    }
                    else
                    {
                        rpmPoints.Add(lineSegments_LS2[_maxBarParts].Point);
                    }
                    break;
            }

            int i = 0;
            gridTicks.Children.Clear();

            //add the ticks to the grid
            //if we have a redline then color accordingly
            foreach (PathGeometry geom in tickPaths)
            {
                Path p = new Path() { Data = geom, StrokeThickness = 1 };

                if ((i >= redLinePoint) && (_redLineStyle != eRedLineStyle.None))
                {
                    p.Stroke = new SolidColorBrush(_redLineColor);
                    p.Fill = new SolidColorBrush(_redLineColor);
                }
                else
                {
                    p.Stroke = new SolidColorBrush(_majorScaleTickColor);
                    p.Fill = new SolidColorBrush(_majorScaleTickColor);
                }

                gridTicks.Children.Add(p);
                i++;
            }

            //we split the path up so we can have two colors
            //if we're not using the redline then we just leave it and the rpmpath will take over the end bits

            Polyline rpmPath = new Polyline() { StrokeThickness = 5, Stroke = new SolidColorBrush(_majorScaleTickColor), Points = rpmPoints };
            gridGaugeSkeleton.Children.Add(rpmPath);
            
            Polyline rpmRedLinePath = new Polyline() { StrokeThickness = 5 };
            if (_redLineStyle != eRedLineStyle.None)
            {
                rpmRedLinePath.Stroke = new SolidColorBrush(_redLineColor);

            }

            rpmRedLinePath.Points = rpmRedLinePoints;
            gridGaugeSkeleton.Children.Add(rpmRedLinePath);
        }

        private static float bezierPoint(float t, float a, float b, float c, float d)
        {
            float C1 = (d - (3 * c) + (3 * b) - a);
            float C2 = ((3 * c) - (6 * b) + (3 * a));
            float C3 = ((3 * b) - (3 * a));
            float C4 = (a);

            return (C1 * t * t * t + C2 * t * t + C3 * t + C4);
        }

        private static float bezierTangent(float t, float a, float b, float c, float d)
        {
            float C1 = (d - (3 * c) + (3 * b) - a);
            float C2 = ((3 * c) - (6 * b) + (3 * a));
            float C3 = ((3 * b) - (3 * a));

            return ((3 * C1 * t * t) + (2 * C2 * t) + C3);
        }

#endregion

    }
}
