﻿using System;
using System.Windows.Data;
using System.Windows.Media;

namespace notMotecGauge
{
    public class VC_ColorToSolidColorBrush : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (null == value)
            {
                return null;
            }

            if (value is Color)
            {
                Color _brush = (Color)value;
                return new SolidColorBrush(_brush);
            }

            Type type = value.GetType();
            throw new InvalidOperationException(String.Format("Unsupported type [{0}]", type.Name));
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}
